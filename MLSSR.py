#! /usr/bin/env python3

# MLSSR
# Copyright (C) 2022 Cyril Conde
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import sys
import os
#import shutil
import subprocess
import argparse
import gzip
import json
#import itertools
from glob import glob

from pathlib import Path

from dataclasses import dataclass, field
from typing import List, Mapping

import numpy
import pandas

from Bio import SeqIO
from Bio import SearchIO
from Bio.SeqRecord import SeqRecord


MLSSR_TABLE = Path(__file__).parent / "MLSSR_loci.tsv"


def extract_loci_and_primer(fasta, out_dir, region_size, primer_size):
    if region_size < primer_size:
        sys.exit("Region surrounding each locus need to be greater than primer_size... Exiting")

    out_dir = Path(out_dir)
    out_dir.mkdir(exist_ok=True)
    
    region_file = out_dir / (f"MLSSR_region_{region_size}.fasta")
    primer_file = out_dir / (f"MLSSR_primer_{primer_size}.fasta")

    genome = SeqIO.read(fasta, "fasta")
    genome_seq = genome.seq

    df = pandas.read_csv(MLSSR_TABLE, sep="\t")
    print(df)

    mlssr_k10 = [19, 10, 5, 5, 5, 5, 5, 5, 5, 5, 5]
    mlssr_test = []

    with open(region_file, "w") as region_out, open(primer_file, "w") as primer_out:
        for row in df.itertuples():
            start, end = [int(p) for p in row.location.split('-')]
            start = start - 1 # Python is 0-based and semi-inclusive [start-1:end[ while position in df are 1-based and inclusive [start:end]
            repeat = genome_seq[start:end]
            locus_region = SeqRecord(genome_seq[start - region_size:end + region_size], id=f"{row.locus}", description=f"{len(genome_seq[start-primer_size:end+primer_size])}")
            primer_1 = SeqRecord(genome_seq[start - primer_size:start], id=f"{row.locus}_1", description=f"{len(genome_seq[start-primer_size:start])}")
            primer_2 = SeqRecord(genome_seq[end:end + primer_size], id=f"{row.locus}_2", description=f"{len(genome_seq[end:end+primer_size])}")
            locus_repeat = len(repeat) // len(row.repeat)
            mlssr_test.append(locus_repeat)
            print(row.locus, start, end, row.repeat, repeat, locus_repeat, locus_region.seq, primer_1.seq, primer_2.seq)
            SeqIO.write(locus_region, region_out, "fasta")
            SeqIO.write(primer_1, primer_out, "fasta")
            SeqIO.write(primer_2, primer_out, "fasta")

    if mlssr_k10 == mlssr_test:
        print("Good profile")
    else:
        print("Bad profile")

    subprocess.run(f"makeblastdb -in {region_file} -dbtype nucl 1>/dev/null", shell=True)
    subprocess.run(f"makeblastdb -in {primer_file} -dbtype nucl 1>/dev/null", shell=True)
    subprocess.run(f"bwa index {region_file} 1>/dev/null", shell=True)


def check_format(input_file):
    with open(input_file, "rb") as fd:
        begin = fd.read(2)

    is_compressed = False
    if begin.hex() == "1f8b":
        is_compressed = True
        with gzip.open(input_file, "rt") as fd:
            begin = fd.read(1)
    else:
        try:
            begin = begin.decode()[0]
        except UnicodeDecodeError:
            return ("unknown", is_compressed)
 
    if begin == "@":
        return ("fastq", is_compressed)
    elif begin == ">":
        return ("fasta", is_compressed)
    else:
        return ("unknown", is_compressed)


def convert_float_to_int_to_str(series):
    result = []
    for elem in series.values:
        # The only way to know if we got a numpy.nan (which can be parse using numpy.float64)
        if elem.__str__() == "nan":
            result.append(str())
        else:
            if isinstance(elem, (float, int)):
                result.append(str(int(elem)))
            else:
                result.append(elem)
    return result


#def any2fasta(input_file, output_handle, input_type, compressed_input=False, tag=None):
#    if compressed_input:
#        input_file = gzip.open(input_file, "rt")
#    for n, fastq in enumerate(SeqIO.parse(input_file, input_type)):
#        print(f"{n}", end="\r", file=sys.stderr)
#
#        if tag and isinstance(tag, str):
#            fastq.id = fastq.id + tag
# 
#        SeqIO.write(fastq, output_handle, "fasta")
#
#    print(f"{n} sequences converted !", file=sys.stderr)
#    if compressed_input:
#        input_file.close()


#def merge_reads(input, output, gz):
#    with open(output, "w") as fasta_out:
#        for i, fastq_file in enumerate(input, 1):
#            input_type, compressed_input = check_format(fastq_file)
#            if input_type == "unknown":
#                sys.exit("Unknown format for input file: {f}")
#            else:
#                any2fasta(fastq_file, fasta_out, input_type, compressed_input, tag=f"/{i}")
#    if gz:
#        subprocess.run(f"gzip -f {output}", shell=True)


def align_extract_reads(reads, ssr_loci, tmp_dir, threads):
    bam = tmp_dir / "aligned_reads_to_ssr_loci.bam"
    bam_index = tmp_dir / "aligned_reads_to_ssr_loci.bam.csi"
    fastq_name = Path(reads[0]).stem.replace(".R1.", ".SSR.")
    fasta_name = fastq_name.replace("fastq", "fasta")
    reads = " ".join(reads) if isinstance(reads, (list, tuple)) else reads
    aligned_reads_fastq = str(tmp_dir / fastq_name)
    aligned_reads_fasta = str(tmp_dir / fasta_name)

    index_cmd = f"bwa index {ssr_loci} 1>/dev/null 2>&1"
    align_cmd = f"bwa mem -t {threads} {ssr_loci} {reads} 2>{str(tmp_dir / 'bwa.log')} | samtools sort -n - | samtools fixmate -m - - | samtools sort - | samtools markdup - - | samtools view -b -o {str(bam)} --write-index -"

    ## If index database file doesn't exists or database file if newer than blast index file then rebuild blast database
    #if not os.path.exists(database + ".nhr") or os.path.getmtime(database) > os.path.getmtime(database + ".nhr"):
    #    print(f"Creating BLAST database from input ({database})", file=sys.stderr)
    #    subprocess.run(f"makeblastdb -in {database} -dbtype nucl 1>/dev/null", shell=True)
    #else:
    #    print(f"BLAST database for {database} already exists", file=sys.stderr)
    print(f"Align reads {reads} to SSR loci {ssr_loci}", file=sys.stderr)
    subprocess.run(index_cmd, shell=True)
    subprocess.run(align_cmd, shell=True)

    print(f"Extract mapped reads from {bam} to {fastq_name}", file=sys.stderr)
    subprocess.run(f"samtools fastq -F 2308 -N {str(bam)} 2>{str(tmp_dir / 'samtools_fastq.log')} > {aligned_reads_fastq}" , shell=True)
    subprocess.run(f"samtools fasta -F 2308 -N {str(bam)} 2>{str(tmp_dir / 'samtools_fasta.log')} > {aligned_reads_fasta}" , shell=True)

    # Clean alignment bam file if reads were extracted
    if os.stat(aligned_reads_fastq).st_size == 0:
        sys.exit("Error: No reads was extracted from the bam file {str(bam)}.. Exiting")
    else:
        print(f"\nCleaning bam file", file=sys.stderr)
        bam.unlink()
        bam_index.unlink()

    return aligned_reads_fastq, aligned_reads_fasta


def blast(query, database, out, evalue="1e-10", word_size=28, pid=100):
    # If index database file doesn't exists or database file if newer than blast index file then rebuild blast database
    if not os.path.exists(database + ".nhr") or os.path.getmtime(database) > os.path.getmtime(database + ".nhr"):
        print(f"Creating BLAST database from input ({database})", file=sys.stderr)
        subprocess.run(f"makeblastdb -in {database} -dbtype nucl 1>/dev/null", shell=True)
    else:
        print(f"BLAST database for {database} already exists", file=sys.stderr)
    print(f"Performing BLAST of primer ({query}) against input {database}", file=sys.stderr)
    subprocess.run(f"blastn -query {query} -db {database} -perc_identity {pid} -evalue {evalue} -word_size {word_size} -outfmt 7 -out {out}", shell=True)
    #subprocess.run(f"blastn -query {query} -db {database} -evalue {evalue} -word_size {word_size} -outfmt 7 -out {out}", shell=True)


@dataclass
class Coordinate:
    # Name
    query_name: str
    hit_name: str
    # Query
    query_start: int
    query_end: int
    query_strand: int
    # Hit
    hit_start: int
    hit_end: int
    hit_strand: int

    @property
    def qs(self):
        return self.query_start

    @property
    def qe(self):
        return self.query_end

    @property
    def hs(self):
        return self.hit_start

    @property
    def he(self):
        return self.hit_end


class CustomJSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, Coordinate):
            dct = {o.__class__.__name__: True}
            dct.update(o.__dict__)
            return dct
        else:
            return super().default(o)


def customJSONDecoder(dct):
    if "Coordinate" in dct:
        del dct["Coordinate"]
        return Coordinate(**dct)
    return dct


def parse_blast_results(blast_result, primer_size, debug=False):
    print(f"Parsing BLAST results from {blast_result}", file=sys.stderr)
    # coordinates dictionary
    # {Locus: {primer_number: [filtered_hsp_list]}}
    coordinates = {}

    # Parse blast results
    # QueryResult   ->    [Hits]    ->      [Hsps]
    # per query         per results         per alignment
    #                   in database/
    #                   reference
    # Direct use of QueryResults.hsps avoid error in case where only one contig is remaining at the stage of circularisation
    # Only 1 hit with many alignment (hsp)
    for record in SearchIO.parse(blast_result, "blast-tab", comments=True):
        query = record.id
        primer_name, primer_id = query.split("_")
        for i, hit in enumerate(record.hits, 1):
            for n, hsp in enumerate(hit.hsps, 1):
                # primer 1 need to be complete at the end because ssr is after this primer
                # primer 2 need to be complete at the begining because ssr is before this primer
                if (primer_id == "1" and hsp.query_range[1] == primer_size) or (primer_id == "2" and hsp.query_range[0] == 0):
                    coords = Coordinate(query, hit.id, *hsp.query_range, hsp.query_strand, *hsp.hit_range, hsp.hit_strand)
                    if coordinates.get(primer_name, False):
                        if coordinates[primer_name].get(primer_id, False):
                            coordinates[primer_name][primer_id].append(coords)
                        else:
                            coordinates[primer_name][primer_id] = [coords]
                    else:
                        coordinates[primer_name] = {primer_id: [coords]}
                    if debug:
                        print(coords)

    return coordinates


@dataclass
class SSRLocus:
    name: str
    motif: str
    ssr: Mapping[str, int] = field(default_factory=dict)
    coords: Mapping[str, List[Coordinate]] = field(default_factory=dict)

    def add_coordinate_from_blast(self, *coordinates):
        for coordinate in coordinates:
            hit_name = coordinate.hit_name

            if not self.coords.get(hit_name, False):
                # if locus name exists, then check if hit exists. If it doesn't exixsts, create a new entry with associated Coordinate
                self.coords[hit_name] = [coordinate]
            elif len(self.coords[hit_name]) == 1: # If hit name exists, then only append Coordinate to the list
                self.coords[hit_name].append(coordinate)
                self.coords[hit_name] = sorted(self.coords[hit_name], key=lambda x: x.he)
                q1, q2 = self.coords[hit_name]
                self.ssr[hit_name] = q2.hs - q1.he
            else:
                print(f"Warning: Trying to add more than 2 coordinates to SSR Locus : {hit_name} ({coordinate})", file=sys.stderr)


#def search_ssr(seq):
#    df = pandas.read_csv(MLSSR_TABLE, sep="\t")
#    #locus2 = re.compile("(?:C{1})*")
#    for row in df.itertuples():
#        locus = re.compile("(?:{row.repeat}{1})*")
#        result = {}
#        for i in locus1.finditer(seq):
#            if i.group():
#                result.append(i, len(i.group()))
#        span = sorted(result, key=lambda x: x[2], reverse=True)[0][0].span()
#        print(seq[span[0]:span[1]])


def fasta2ssr(fasta, out_dir, prefix, ssr_loci, ssr_primer, tmp_dir, threads):
    # Common step from here: work on fasta
    basename = Path(fasta).name

    # Get primer size for calculation of the blast option
    primer_size = len(next(SeqIO.parse(ssr_primer, "fasta")).seq)
    pid = round((((primer_size - 1) / primer_size) * 100) - 0.001, 3)
    if primer_size < (28 + 10):
        word_size = primer_size - 10
        if word_size < 4:
            word_size = 4
        evalue="1e-1"
    else:
        word_size = 28
        evalue="1e-10"

    print(f"Primer size = {primer_size} so BLASTn word_size option will be {word_size}, perc_identity will be {pid} and evalue option will be {evalue}")

    blast_file = str(tmp_dir / "mlssr.blast")
    blast(ssr_primer, fasta, blast_file, evalue=evalue, word_size=word_size, pid=pid)
    coords = parse_blast_results(blast_file, primer_size, debug=False)

    blast_json = str(tmp_dir / "mlssr.json")
    with open(blast_json, "w") as mlssr_blast_result:
        json.dump(coords, mlssr_blast_result, indent=4, cls=CustomJSONEncoder)

    #with open(blast_json) as mlssr_blast_result:
    #    coords = json.load(mlssr_blast_result, object_hook=customJSONDecoder)

    print(f"Converting BLAST results into SSR Loci", file=sys.stderr)
    df_table_ssr = pandas.read_csv(MLSSR_TABLE, sep="\t").set_index("locus", drop=False)

    print(f"Make results", file=sys.stderr)
    df_raw_result_mlssr = pandas.DataFrame(columns=list(df_table_ssr["locus"].values))
    df_result_mlssr = pandas.DataFrame(columns=list(df_table_ssr["locus"].values))

    for row in df_table_ssr.itertuples():
        ssr_locus = SSRLocus(row.locus, row.repeat)
        blast_result_locus = coords.get(row.locus, [])
        if len(blast_result_locus) != 2:
            print(f"Warning ! Bad number of result for primer: {blast_result_locus}... Ignoring", file=sys.stderr)
            continue
        dct_idx = [{c.hit_name:n for n, c in enumerate(blast_result_locus[i])} for i in blast_result_locus]
        intersect = list(set(dct_idx[0].keys()).intersection(set(dct_idx[1].keys())))
        for paired_hit_name in intersect:
            for primer_id, dct in zip(blast_result_locus.keys(), dct_idx):
                ssr_locus.add_coordinate_from_blast(blast_result_locus[primer_id][dct[paired_hit_name]])

        for n, ssr in enumerate(ssr_locus.ssr.values()):
            # Size of the SSR divide by length of the repeat equals to number of repeat
            ssr_result = ssr // len(row.repeat)
            df_raw_result_mlssr.at[n, row.locus] = ssr
            df_result_mlssr.at[n, row.locus] = ssr_result

    print(f"Final Output", file=sys.stderr)
    df_most_common_mlssr = df_result_mlssr.mode()
    # Same as in the publication
    #df_most_common_mlssr.apply(lambda s: ["+" if e is not numpy.nan and e > 11 else e for e in s.values], axis=1)
    # Avoid misrepresentation of the SSR profile
    df_most_common_mlssr = df_most_common_mlssr.apply(convert_float_to_int_to_str)
    df_most_common_mlssr["Data"] = basename
    
    # Output data
    raw_result_file = out_dir / (prefix + ".raw.tsv")
    result_file = out_dir / (prefix + ".tsv")
    most_common_result_file = out_dir / (prefix + ".most_common.tsv")
    #ext = os.path.splitext(output)[1]
    #df_raw_result_mlssr.to_csv(output.replace(ext, f".raw{ext}"), sep="\t", index=False)
    #df_result_mlssr.to_csv(output, sep="\t", index=False)
    #df_most_common_mlssr.to_csv(output.replace(ext, f".most_common{ext}"), sep="\t", index=False)
    df_raw_result_mlssr.to_csv(raw_result_file, sep="\t", index=False)
    df_result_mlssr.to_csv(result_file, sep="\t", index=False)
    df_most_common_mlssr.to_csv(most_common_result_file, sep="\t", index=False)

    # Print data
    print("\nRaw length for each SSR Locus", file=sys.stderr)
    print(df_raw_result_mlssr)
    print("\nRepeat number for each SSR Locus", file=sys.stderr)
    print(df_result_mlssr)
    print("\nMost Common repeat number for each SSR Locus", file=sys.stderr)
    print(df_most_common_mlssr)

    # Clean blast database
    print(f"\nCleaning blast database", file=sys.stderr)
    blast_db_files = glob(f"{fasta}.*")
    for db_file in blast_db_files:
        os.remove(db_file)


def local_reads_assembly(reads, tmp_dir, threads):
    output_dir = tmp_dir / "assembly"
    output_contig = str(output_dir / "contigs.fasta")
    output_contig_renamed = str(output_dir / (str(Path(reads).name).split('.')[0] + "_contigs.fasta"))
    print(f"Assemble extracted reads {reads} that mapped to SSR loci in order to check concordance", file=sys.stderr)
    subprocess.run(f"spades.py --careful -o {str(output_dir)} -t {threads} -s {reads} 1>{str(tmp_dir / 'spades.log')} 2>&1", shell=True)
    os.rename(output_contig, output_contig_renamed)
    return output_contig_renamed


def MLSSR(input_data, out_dir, prefix, ssr_loci, ssr_primer, temp_dir, threads):
    out_dir = Path(out_dir)
    out_dir.mkdir(exist_ok=True)
    tmp_dir = out_dir / temp_dir
    tmp_dir.mkdir(exist_ok=True)
    data_dir = out_dir / "data"
    data_dir.mkdir(exist_ok=True)

    # Make symbolic link to avoid concurrent access in write mode to bwa index files
    file_to_symlink = input_data + [ssr_loci, ssr_primer]
    for i, f in enumerate(file_to_symlink):
        f_basename = Path(f).name
        src = str(Path(f).resolve())
        dst = str(data_dir / f_basename)
        try:
            os.symlink(src, dst)
        except FileExistsError:
            pass
        file_to_symlink[i] = dst

    # If paired-end reads are given as input
    if len(input_data) > 1:
        R1, R2, ssr_loci, ssr_primer = file_to_symlink
        input_data = [R1, R2]
        del R1, R2
    else:
        assembly, ssr_loci, ssr_primer = file_to_symlink
        input_data = [assembly]
        del assembly

    # Parse input and find type (complete/draft Genome (fasta) or reads (fasta/fastq/gz)
    input_type, compressed = check_format(input_data[0])

    if input_type == "unknown":
        sys.exit("Unknown format for input file: {input_data[0]}")
    elif input_type == "fasta":
        if len(input_data) > 1:
            sys.exit("Please give only one fasta file: {input_data}")
        input_data = input_data[0]
        if compressed:
            # uncompress fasta
            print(f"Uncompressing input {input_type} {input_data}", file=sys.stderr)
            subprocess.run(f"gunzip {input_data}", shell=True)
            input_data = Path(input_data)
            # Remove .gz
            fasta = str(input_data.parent / input_data.stem)
        else:
            fasta = input_data
    else:
        # Reads for mapping and assembly
        fastq, fasta = align_extract_reads(input_data, ssr_loci, tmp_dir, threads)

    fasta2ssr(fasta, out_dir, prefix, ssr_loci, ssr_primer, tmp_dir, threads)

    # Assemble reads mapped to ssr loci in order to check concordance
    if input_type == "fastq":
        tmp_assembly_dir = out_dir / (temp_dir + "_assembly")
        tmp_assembly_dir.mkdir(exist_ok=True)
        output_contig = local_reads_assembly(fastq, tmp_assembly_dir, threads)
        prefix_assembly = prefix + "_assembly"
        fasta2ssr(output_contig, out_dir, prefix_assembly, ssr_loci, ssr_primer, tmp_assembly_dir, threads)

        # Correct the result if necessary
        most_common_result_reads = out_dir / (prefix + ".most_common.tsv")
        df_reads = pandas.read_csv(most_common_result_reads, sep="\t")
        most_common_result_assembly = out_dir / (prefix_assembly + ".most_common.tsv")
        df_assembly = pandas.read_csv(most_common_result_assembly, sep="\t")

        df_merged = pandas.DataFrame(columns=df_reads.columns)

        # Compare results from reads and assembly
        for locus, series in df_reads[df_reads.columns[:11]].T.iterrows():
            reads_values = [n for n in series.values if n.__str__() != "nan"]
            assembly_values = [n for n in df_assembly[locus].values if n.__str__() != "nan"]
            print(f"Reads_values: {len(reads_values)}")
            print(f"Assembly_values: {len(assembly_values)}")
            if assembly_values == reads_values:
                print(locus, "Equal. Leave unchange.")
                for i, r in enumerate(assembly_values):
                    df_merged.loc[i, locus] = r
            elif assembly_values != reads_values:
                if len(assembly_values) == 1 and len(reads_values) == 1:
                    print(locus, "Reads and assembly are differente with the same length. Take assembly result.")
                    df_merged[locus] = assembly_values
                elif len(assembly_values) != 1 and len(reads_values) == 1:
                    print(locus, "Reads and assembly are differente. Assembly is empty or ambiguous. Take reads result.")
                    df_merged[locus] = reads_values
                elif len(assembly_values) == 1 and len(reads_values) != 1:
                    print(locus, "Reads and assembly are differente. Reads is empty or ambiguous. Take assembly result.")
                    df_merged[locus] = assembly_values
                elif len(assembly_values) > 1 and len(reads_values) > 1:
                    print(locus, "Reads and assembly are differente. Both ambiguous. Take assembly result.")
                    for i, r in enumerate(assembly_values):
                        df_merged.loc[i, locus] = r
            else:
                print("Unknown case.")

        df_merged["Data"] = df_reads["Data"].values[0]

        df_merged = df_merged.apply(convert_float_to_int_to_str)
        print(df_merged)
        most_common_result_merged = out_dir / ".merged.tsv"
        df_merged.to_csv(most_common_result_merged, sep="\t", index=False)


def compile_results(paths, database, output):
    df_mlssr_all = pandas.DataFrame()
    for path in paths:
        df = pandas.read_csv(path, sep="\t")
        df = df.fillna(numpy.nan)
        df_mlssr_all = df_mlssr_all.append(df, ignore_index=True, sort=False)

    # Convert all type to str
    df_mlssr_all = df_mlssr_all.apply(convert_float_to_int_to_str)
    # Same as in the publication
    df_mlssr_all[df_mlssr_all.columns[:11]] = df_mlssr_all[df_mlssr_all.columns[:11]].apply(lambda s: ["+" if e and int(e) > 11 else e for e in s.values])
    # Add sample name
    df_mlssr_all["sample"] = df_mlssr_all["Data"].apply(lambda e: e.split("_")[0])
    # Copy SSR for loci with multiple possibilities
    for name, group in df_mlssr_all.groupby("sample"):
        if len(group) > 1:
            for col_name, series in group.items():
                col_values = series.values
                for index, col_value in series.items():
                    if not col_value and col_values[0]:
                        df_mlssr_all.loc[index, col_name] = col_values[0]

    # Add profile
    db_ssr = pandas.read_csv(database, sep="\t")
    df_mlssr_all["codenum"] = df_mlssr_all[df_mlssr_all.columns[:11]].apply(lambda s: "".join([str(v) for v in s.values]), axis=1)
    df_mlssr_all = df_mlssr_all.merge(db_ssr[["MLSSR", "codenum"]], how="left", on="codenum")

    df_mlssr_all.to_csv(output, sep="\t", index=False)
    df_mlssr_all.to_excel(output.replace(os.path.splitext(output)[1], ".xlsx"), index=False)

    #Output unique entries and unique entries with assigned profile
    df_mlssr_uniq = pandas.DataFrame()
    df_mlssr_uniq_ssr = pandas.DataFrame()
    for name, group in df_mlssr_all.groupby("sample"):
        if len(group) == 1:
            df_mlssr_uniq = df_mlssr_uniq.append(group, ignore_index=True, sort=False)
            if group.iloc[0, -1].__str__() != "nan":
                df_mlssr_uniq_ssr = df_mlssr_uniq_ssr.append(group, ignore_index=True, sort=False)

    df_mlssr_uniq.to_csv(output.replace(os.path.splitext(output)[1], f".uniq{os.path.splitext(output)[1]}"), sep="\t", index=False)
    df_mlssr_uniq_ssr["MLSSR"] = df_mlssr_uniq_ssr["MLSSR"].apply(lambda e: str(int(e)))
    df_mlssr_uniq_ssr.to_csv(output.replace(os.path.splitext(output)[1], f".uniq.ssr{os.path.splitext(output)[1]}"), sep="\t", index=False)
    #del df_mlssr_all["Data"]
    #df_stat = df_mlssr_all.value_counts(normalize=True)
    #print(df_mlssr_all.describe().to_string())
    #print(df_mlssr_all["Locus4"].unique())
    #print(df_mlssr_all.count().to_string())


class Register:
    def __init__(self, parser_description="", subparser_help=""):
        self.parser = argparse.ArgumentParser(parser_description)
        self.subparser = self.parser.add_subparsers(dest="command", help=subparser_help)
        self.subparser_map = dict()
        self.function_register = dict()

    def register(self, func, subparser_name, subparser_help=""):
        self.subparser_map[subparser_name] = self.subparser.add_parser(subparser_name, help=subparser_help)
        self.function_register[subparser_name] = func
        return self.subparser_map[subparser_name]

    def parse(self, func=None):
        args = vars(self.parser.parse_args())
        command = args.pop("command", None)
        if func:
            args = func(args)
        if command:
            try:
                self.function_register[command](**args)
            except Exception as e:
                print(e, file=sys.stderr)
                raise e
        else:
            print("Unknown command... Exiting")


if __name__ == '__main__':
    programRegister = Register(subparser_help="Available command:")

    parser_loci = programRegister.register(extract_loci_and_primer, 'locus', subparser_help='Extract SSR loci region from a reference genome and design primer for MLSSR.')
    parser_loci.add_argument("-f", "--fasta", required=True, help="Complete reference genome in fasta format.")
    parser_loci.add_argument("-d", "--out_dir", required=True, help="Path to output directory.")
    parser_loci.add_argument("-r", "--region_size", type=int, default=100, help="Region size surrounding SSR Loci. [100]")
    parser_loci.add_argument("-s", "--primer_size", type=int, default=30, help="Primer size for SSR Loci genotyping. [30]")

    #parser_merge = programRegister.register(merge_reads, 'merge', subparser_help='Merge paired ends reads into one fasta file.')
    #parser_merge.add_argument("-i", "--input", nargs="+", required=True, help="Reads to merge. Can be gzip compressed.")
    #parser_merge.add_argument("-o", "--output", required=True, help="Output path in fasta format.")
    #parser_merge.add_argument("--gzip", dest="gz", action="store_true", help="Compress output file?")

    parser_MLSSR = programRegister.register(MLSSR, 'mlssr', subparser_help='Perform \033[3min silico\033[0m MLSSR.')
    parser_MLSSR.add_argument("-i", "--input", dest="input_data", nargs="+", required=True, help="Complete/Draft assembled genome in fasta format or Reads in fasta/fastq format (gzip compressed or not).")
    parser_MLSSR.add_argument("-d", "--out_dir", required=True, help="Path to output directory.")
    parser_MLSSR.add_argument("-p", "--prefix", default="MLSSR", help="Prefix of output files.")
    parser_MLSSR.add_argument("-l", "--ssr_loci", default=f"{Path(__file__).resolve().parent.parent.parent / 'Ref/MLSSR/MLSSR_region_100.fasta'}", help="Loci use to align reads in fasta format.")
    parser_MLSSR.add_argument("-s", "--ssr_primer", default=f"{Path(__file__).resolve().parent.parent.parent / 'Ref/MLSSR/MLSSR_primer.fasta'}", help="Primer use to locate SSR Loci in fasta format.")
    parser_MLSSR.add_argument("-t", "--temp_dir", default="tmp", help="Name of the temporary directory use to store working file.")
    #parser_MLSS2.add_argument("--gzip", dest="gz", action="store_true", help="Compress input file?")
    parser_MLSSR.add_argument("-w", "--threads", type=int, default=2, help="Number of threads given to external tools (bwa and spades). [2]")

    parser_compile = programRegister.register(compile_results, 'compile', subparser_help='Compile \033[3min silico\033[0m MLSSR results.')
    parser_compile.add_argument("-p", "--paths", nargs="+", required=True, help="Path to each most common result file produce by mlssr command.")
    parser_compile.add_argument("-d", "--database", required=True, help="Path to profile's database.")
    parser_compile.add_argument("-o", "--output", default="", help="Output result.")

    programRegister.parse()
