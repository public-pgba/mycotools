# MycoTools

## Description
This repository contains tools for identification and genotyping of mycobacteria.

## Installation
### Dependencies
- [ ] Python (3.7+)
- [ ] Biopython (1.76+)
- [ ] Numpy
- [ ] Pandas
- [ ] NCBI-Blast+
- [ ] BWA
- [ ] SPAdes
- [ ] seqtk

### Installation
Clone the respository.
```
git clone https://forgemia.inra.fr/public-pgba/mycotools.git
```

Install dependencies via conda environnment file provided.
```
cd mycotools
conda env create -f environment.yml
```
You can also install dependencies manually.

## Usage
### MLSSR
This tools contains 3 commands listed below:
```
python3 MLSSR.py -h
usage: [-h] {locus,mlssr,compile} ...

positional arguments:
  {locus,mlssr,compile}
                        Available command:
    locus               Extract SSR loci region from a reference genome and
                        design primer for MLSSR.
    mlssr               Perform in silico MLSSR.
    compile             Compile in silico MLSSR results.

optional arguments:
  -h, --help            show this help message and exit
```

#### locus
```
python3 MLSSR.py locus -h
usage:  locus [-h] -f FASTA -d OUT_DIR [-r REGION_SIZE] [-s PRIMER_SIZE]

optional arguments:
  -h, --help            show this help message and exit
  -f FASTA, --fasta FASTA
                        Complete reference genome in fasta format.
  -d OUT_DIR, --out_dir OUT_DIR
                        Path to output directory.
  -r REGION_SIZE, --region_size REGION_SIZE
                        Region size surrounding SSR Loci. [100]
  -s PRIMER_SIZE, --primer_size PRIMER_SIZE
                        Primer size for SSR Loci genotyping. [30]
```

First, you need to generate the necessary data for MLSSR to work using the locus command:
```
python3 MLSSR.py locus -f K10.fasta -d MLSSR_data -s 30 -r 100
```
This command will use the K-10 genome to extract 100 bp surrounding each SSR locus and design primer of 30 bp.
The output files will be located under the `MLSSR_data` directory.

#### mlssr
```
python3 MLSSR.py mlssr -h
usage:  mlssr [-h] -i INPUT_DATA [INPUT_DATA ...] -d OUT_DIR [-p PREFIX]
              [-l SSR_LOCI] [-s SSR_PRIMER] [-t TEMP_DIR] [-w THREADS]

optional arguments:
  -h, --help            show this help message and exit
  -i INPUT_DATA [INPUT_DATA ...], --input INPUT_DATA [INPUT_DATA ...]
                        Complete/Draft assembled genome in fasta format or
                        Reads in fasta/fastq format (gzip compressed or not).
  -d OUT_DIR, --out_dir OUT_DIR
                        Path to output directory.
  -p PREFIX, --prefix PREFIX
                        Prefix of output files.
  -l SSR_LOCI, --ssr_loci SSR_LOCI
                        Loci use to align reads in fasta format.
  -s SSR_PRIMER, --ssr_primer SSR_PRIMER
                        Primer use to locate SSR Loci in fasta format.
  -t TEMP_DIR, --temp_dir TEMP_DIR
                        Name of the temporary directory use to store working
                        file.
  -w THREADS, --threads THREADS
                        Number of threads given to external tools (bwa and
                        spades). [2]
```

This is the core command which perform the *in silico* SSR typing using previoulsy generate data from locus command.
The typing is conduct as follow:
1. It first align reads against each SSR locus and then extract mapped reads in fasta format.
2. Primers are blast against reads to perform SSR typing directly from reads.
3. Reads assembly is performed to locally reconstruct each locus sequence.
4. Primers are blast against the assembly to perform SSR typing on
5. Finally, result from assembly is compared to result get from reads. If there are non concordante results, assembly is taken as the final result.
```
python3 MLSSR.py mlssr -i reads.R1.fastq.gz reads.R2.fastq.gz -d Results_dir -p MLSSR -l MLSSR_data/MLSSR_region_100.fasta -s MLSSR_data/MLSSR_primer_30.fasta -t tmp -w 4
```
You can pass as input data reads in fastq/fasta and genome (draft or complete) in fasta format (all file can be gzip-compressed). Only step 4 is conduct if a genome in fasta format is passed as input.
Results files will start with the prefix `MLSSR` and will be output under the `Results_dir` directory.
Temporary directory will be named `tmp` under the `Results_dir` directory and will store log files and temporary results of each tool used for the typing.

Main results containning the number of repeats found at each locus are output to `*.most_common.tsv` under the `Results_dir` directory.

#### compile
```
python3 MLSSR.py compile -h
usage:  compile [-h] -p PATHS [PATHS ...] -d DATABASE [-o OUTPUT]

optional arguments:
  -h, --help            show this help message and exit
  -p PATHS [PATHS ...], --paths PATHS [PATHS ...]
                        Path to each most common result file produce by mlssr
                        command.
  -d DATABASE, --database DATABASE
                        Path to profile's database.
  -o OUTPUT, --output OUTPUT
                        Output result.
```

If you performed SSR typing on many isolates, you can use this command to compile the results.
Database can be retrieve from this link: [MAC-INMV-SSR Database](http://mac-inmv.tours.inra.fr/index.php?lang=en)
You need to export the database in TSV format.

### MLVA
We do not recommend using this script because MLVA loci are sometimes longer than the read length. The results should be taken with caution.

## Support
Feel free to open an issue.

## Authors and acknowledgment
Created by Cyril Conde.

## License
![Licence GPL GNUv3](GPL.png)
```
Copyright (C) 2022 Cyril Conde
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

```
