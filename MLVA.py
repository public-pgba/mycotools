#! /usr/bin/env python3

# MLVA
# Copyright (C) 2022 Cyril Conde
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import sys
import os
import argparse
import subprocess
import shutil
import math
from pathlib import Path
import numpy
import pandas
from Bio import SearchIO
from Bio.SeqRecord import SeqRecord


MLVA_TABLE = Path(__file__).parent / "MLVA_loci.tsv"


def buildLociDatabase(fasta, output, primer_size):
    genome = SeqIO.read(fasta, "fasta")
    genome_seq = genome.seq

    loci_table = pandas.read_csv(MLVA_TABLE, sep="\t")
    print(loci_table)

    with open(output, "w") as fasta_out:
        for row in loci_table.itertuples():
            primer = SeqRecord(genome_seq[row.start - primer_size:row.start], id=f"{row.locus}", description=f"{genome.id}|{row.start - primer_size}-{row.start}")
            SeqIO.write(primer, fasta_out, "fasta")


def parse_blast_results(blast_result):
    print(f"Parsing BLAST results from {blast_result}", file=sys.stderr)
    read_names = {}

    # Parse blast results
    # QueryResult   ->    [Hits]    ->      [Hsps]
    # per query         per results         per alignment
    #                   in database/
    #                   reference
    # Direct use of QueryResults.hsps avoid error in case where only one contig is remaining at the stage of circularisation
    # Only 1 hit with many alignment (hsp)
    for record in SearchIO.parse(blast_result, "blast-tab", comments=True):
        query = record.id
        for i, hit in enumerate(record.hits, 1):
            for n, hsp in enumerate(hit.hsps, 1):
                if read_names.get(query, False):
                    read_names[query].append(hit.id)
                else:
                    read_names[query] = [hit.id]

    return read_names


#def get_reads_with_repeat(fastq, primers, output_dir):
#    output_dir = Path(output_dir)
#    if not output_dir.exists():
#        output_dir.mkdir()
#    output_fasta = output_dir / str("reads.fasta")
#    #if output_fasta.exists():
#    #    output_fasta.unlink()
#    #for f in fastq:
#    subprocess.run(f"seqtk seq -A {fastq} > {output_fasta}", shell=True)
#    subprocess.run(f"makeblastdb -in {output_fasta} -dbtype nucl 1>/dev/null 2>&1", shell=True)
#    output_blast = output_dir / "primers_on_reads.blast"
#    subprocess.run(f"blastn -query {primers} -db {output_fasta} -outfmt 7 -word_size 16 -out {output_blast}", shell=True)
#    MLVA_profile = []
#    copy_number = {}
#    for primer, reads in parse_blast_results(output_blast).items():
#        reads_list_file = output_dir / str(primer + "_reads.txt")
#        with open(reads_list_file, "w") as out:
#            for r in reads:
#                print(r, file=out)
#        output_reads = output_dir / str(primer + "_reads.fasta")
#        subprocess.run(f"seqtk subseq {output_fasta} {reads_list_file} > {output_reads}", shell=True)
#        trf_result = output_reads.name + ".2.7.7.80.10.50.500.dat"
#        subprocess.run(f"trf {output_reads} 2 7 7 80 10 50 500 -h -d {trf_result} 1>/dev/null 2>&1", shell=True)
#        with open(trf_result) as trf:
#            copy_number[primer] = []
#            #copy_number = [line.split(" ")[3] for line in trf.read().splitlines()[8:] if line or "Parameters" not in line or "Sequence" not in line]
#            for line in trf.read().splitlines()[8:]:
#                if line and not line.startswith("Parameters") and not line.startswith("Sequence"):
#                    cn = float(line.split(" ")[3])
#                    copy_number[primer].append(cn)
#            if copy_number[primer]:
#                MLVA_profile.append(round(numpy.mean([round(i, 0) for i in copy_number[primer]]), 0))
#            else:
#                MLVA_profile.append("NA")
#    print(copy_number)
#    print(MLVA_profile)


def run_get_reads_with_repeat(output_fasta, repeats, output_blast, debug=False):
    subprocess.run(f"blastn -query {repeats} -db {output_fasta} -outfmt 7 -word_size 11 -perc_identity 90 -qcov_hsp_perc 50 -out {output_blast}", shell=True)
    df = pandas.read_csv(output_blast, sep="\t", comment="#", header=None)
    # Used to retrieve result after groupby
    df_idx = df.set_index([0, 1]).sort_index()
    df1 = df.groupby(0)[1].value_counts()
    for locus in ["292", "X3", "25", "47", "3", "7", "10", "32"]:
        # If repeat follow each other in blast results for each locus
        # Specificity throught repeat chaining - allow plus or minus 5 bp for alignment bias
        # Unspecific repeat in blast result are spaced by more than 5 bp (around 10 bp)
        # Select reads that have the max number of results
        df2 = df1[locus][df1[locus] == df1[locus].max()]
        for read, _ in df2.iteritems():
            # Retrieve raw blast results for a particular read
            df_reads = df_idx.loc[(locus, read)].reset_index()
            # Get blast coordinates on reads, combine start and stop and sort them in ascending order
            coords_results = sorted(list(df_reads[8].values) + list(df_reads[9].values))
            # If chaining is clear then keep read else remove it and continue with the next read
            for i in range(2, len(coords_results), 2):
                coord_prev_i = coords_results[i - 1]
                coord_i = coords_results[i]
                if coord_i - 5 <= coord_prev_i <= coord_i + 5:
                    continue
                else:
                    if debug:
                        print("Read", read, "dropped from locus", locus, "- Coordinates :", coords_results)
                    df1 = df1.drop(read, level=1)
                    break # Stop here and continue with the next read
        # Finally, output the max number of repeat observed on reads
        try:
            print(locus, df1.loc[locus].max(), sep="\t")
        except KeyError:
            print(locus, "NA", sep="\t")


def get_reads_with_repeat(fastq, repeats, output_dir, debug=False):
    output_dir = Path(output_dir)
    output_dir.mkdir(exist_ok=True)
    for i, fq in enumerate(fastq, 1):
        print(f"Run genotyping on {fq}")
        output_fasta = output_dir.joinpath(f"reads_{i}.fasta")
        if not output_fasta.exists() or os.path.getmtime(fq) > os.path.getmtime(output_fasta):
            subprocess.run(f"seqtk seq -A {fq} > {output_fasta}", shell=True)
            subprocess.run(f"makeblastdb -in {output_fasta} -dbtype nucl 1>/dev/null 2>&1", shell=True)
        output_blast = output_dir.joinpath(f"repeat_on_reads_{i}.blast")
        run_get_reads_with_repeat(output_fasta, repeats, output_blast, debug=False)


def alignReads(mlva_primers_dir, mlva_size, fastq, output_dir, prefix, threads):
    fastqR1 = fastq[0]
    fastqR2 = fastq[1]

    primer_dir = Path(mlva_primers_dir)
    primer_list = list(primer_dir.glob('*.fasta'))

    output_dir = Path(output_dir)
    if not output_dir.exists():
        output_dir.mkdir()
    output_fastaR1 = output_dir / str(prefix + ".R1.fasta")
    output_fastaR2 = output_dir / str(prefix + ".R2.fasta")

    subprocess.run(f"seqtk seq -A {fastqR1} > {output_fastaR1}", shell=True)
    subprocess.run(f"seqtk seq -A {fastqR2} > {output_fastaR2}", shell=True)

    subprocess.run(f"makeblastdb -in {output_fastaR1} -dbtype nucl", shell=True)
    subprocess.run(f"makeblastdb -in {output_fastaR2} -dbtype nucl", shell=True)

    df = pandas.read_csv(mlva_size, sep="\t").set_index("name")
    blast_files = []

    for primer_file in primer_list:
        primer_name = str(primer_file.stem)

        output_fastq = output_dir / str(prefix + f".R1R2_{primer_name}.fastq")
        output_dir_SPAdes = output_dir / str(prefix + f"_assembly_{primer_name}")
        if output_dir_SPAdes.exists():
            shutil.rmtree(output_dir_SPAdes)
        output_contigs = output_dir_SPAdes / "contigs.fasta"
        output_blast = output_dir / str(prefix + f"_{primer_name}.blast")

        #subprocess.run(f"blastn -query {str(primer_file)} -db {output_fastaR1} -word_size 18 -outfmt 6 | sed 's/\t/ /g' | cut -d ' ' -f 2 | seqtk subseq {fastqR1} - > {output_fastq}", shell=True)
        #subprocess.run(f"blastn -query {str(primer_file)} -db {output_fastaR2} -word_size 18 -outfmt 6 | sed 's/\t/ /g' | cut -d ' ' -f 2 | seqtk subseq {fastqR2} - >> {output_fastq}", shell=True)

        subprocess.run(f"spades.py -o {str(output_dir_SPAdes)} --careful -t 2 -s {str(output_fastq)}", shell=True)
        subprocess.run(f"makeblastdb -in {str(output_contigs)} -dbtype nucl", shell=True)
        subprocess.run(f"blastn -query {str(primer_file)} -db {str(output_contigs)} -word_size 18 -outfmt 7 -out {str(output_blast)}", shell=True)
        
        blast_files.append(output_blast)

    for blast_file in blast_files:
        primer_name = blast_file.stem.split("_")[-1]
        blast_results = SearchIO.parse(blast_file, "blast-tab", comments=True)
        primer_coordinates = [coordinate for result in blast_results for coordinate in result.hsps[0].hit_range]
        #print(primer_coordinates)
        pcr_size = max(primer_coordinates) - min(primer_coordinates)
        flanking_size = df.loc[primer_name, "flanking"]
        repeat_size = df.loc[primer_name, "repeat"]

        repeat_number = (pcr_size - flanking_size) / repeat_size
        print(primer_name, repeat_number, sep="\t", end="\t")
        if math.ceil(repeat_number) == repeat_number:
            print("Entier : ", repeat_number)
            continue
        elif repeat_number - 0.5 > int(repeat_number):
            print("Arrondi supérieur : ", math.ceil(repeat_number))
            continue
        elif round(repeat_number, 1) - int(repeat_number) == 0.5:
            print("Demi répétition : ", round(repeat_number, 1))
        else:
            print("Arrondi inférieur : ", math.floor(repeat_number))
            continue

    # TODO: rm temporary file


class Register:
    def __init__(self, parser_description="", subparser_help=""):
        self.parser = argparse.ArgumentParser(parser_description)
        self.subparser = self.parser.add_subparsers(dest="command", help=subparser_help)
        self.subparser_map = dict()
        self.function_register = dict()

    def register(self, func, subparser_name, subparser_help=""):
        self.subparser_map[subparser_name] = self.subparser.add_parser(subparser_name, help=subparser_help)
        self.function_register[subparser_name] = func
        return self.subparser_map[subparser_name]

    def parse(self):
        args = vars(self.parser.parse_args())
        command = args.pop("command", None)
        if command:
            self.function_register[command](**args)
        else:
            print("Unknown command... Exiting")


if __name__ == '__main__':
    programRegister = Register()

    build_parser = programRegister.register(buildLociDatabase, "build", subparser_help="Extract and build the MLVA loci database.")
    build_parser.add_argument("-g", "--genbank", type=str, required=True, help="Genbank file of the reference strain.")
    build_parser.add_argument("-o", "--output", type=str, required=True, help="MLVA loci output file name.")
    build_parser.add_argument("-s", "--primer_size", type=int, default=30, help="Size of primer used to select reads. [30]")

    align_parser = programRegister.register(alignReads, 'align', subparser_help='Align reads to MLVA loci of the reference.')
    align_parser.add_argument("-m", "--mlva_primers_dir", type=str, required=True, help="Directory containing primer pair for each locus (one primer pair per locus).")
    align_parser.add_argument("-s", "--mlva_size", type=str, required=True, help="")
    align_parser.add_argument("-f", "--fastq", type=str, nargs="+", help="Reads of the strain.")
    align_parser.add_argument("-d", "--output_dir", type=str, required=True, help="MLVA loci output directory.")
    align_parser.add_argument("-p", "--prefix", type=str, required=True, help="Output file name prefix.")
    align_parser.add_argument("-t", "--threads", type=str, default=2, help="Number of threads. [2]") 

    MLVA_parser = programRegister.register(get_reads_with_repeat, 'reads', subparser_help='Perform MLVA typing directly from read file.')
    MLVA_parser.add_argument("-r", "--repeats", type=str, required=True, help="Fasta with repeated unit sequence for each locus (one sequence per locus).")
    #MLVA_parser.add_argument("-p", "--primers", type=str, required=True, help="Primer each locus (one primer per locus).")
    #MLVA_parser.add_argument("-f", "--fastq", required=True, help="Read file in fastq format.")
    MLVA_parser.add_argument("-f", "--fastq", nargs="+", help="Reads file in fastq format.")
    MLVA_parser.add_argument("-d", "--output_dir", type=str, required=True, help="Results output directory.")
    MLVA_parser.add_argument("--debug", action="store_true", help="Print debugging messages.")

    programRegister.parse()
